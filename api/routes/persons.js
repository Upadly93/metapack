const express = require('express');
const router = express.Router();
const redis = require('redis');
const client = redis.createClient();
const Ajv = require('ajv');
const ajv = new Ajv({
    allErrors: true
});
const cache = require('memory-cache');
const mongoose = require('mongoose');

const checkAuth = require('../middleware/check-aut');
const Person = require('../models/person');

/**
 * Method to create record
 */
router.post('/', checkAuth, (req, res, next) => {
    const body = req.body;
    const personBody = {
        name: body.name,
        age: body.age,
        email: body.email,
        phone: body.phone
    };

    const personSchema = {
        "properties": {
            name: {
                "type": "string",
                "minLength": 1,
                "maxLength": 30,
                "pattern": "^[a-zA-Z\\s]+$"
            },
            age: {
                "type": "number",
                "minimum": 0,
                "maximum": 200
            },
            email: {
                "type": "string",
                "pattern": "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
            },
            phone: {
                "type": "string",
                "pattern": "^\\(?([0-9]{3})\\)?([ .-]?)([0-9]{3})\\2([0-9]{3})$"
            }
        },
        "required": ["name", "age", "email"]
    }

    const validate = ajv.compile(personSchema);
    const valid = validate(personBody);

    if (ajv.errorsText(validate.errors) === "No errors") {
        const person = new Person({
            _id: new mongoose.Types.ObjectId(),
            name: personBody.name,
            age: personBody.age,
            email: personBody.email,
            phone: personBody.phone
        });

        person.save().then(result => {
            res.status(200).json({
                message: 'Record was created correctly',
                data: result
            });
            console.log(result);
        }).catch(err => {
            res.status(500).json({
                message: 'Something wen\'t wrong'
            });
            console.log(err);
        });
    } else {
        res.status(500).json({
            message: ajv.errorsText(validate.errors)
        });
    }
});

/**
 * Method to getting collection
 */
router.get('/', checkAuth, (req, res, next) => {
    Person.find((err, response) => {
        console.log(response);
        console.log(err);
        if (err === null) {
            res.status(200).json({
                message: "Gettin collection from database",
                data: response
            });
        } else {
            res.status(500).json({
                message: 'Something wen\'t wrong'
            });
        }
    });
});

/**
 * Method to getting a record
 */
router.get('/:personId', checkAuth, (req, res, next) => {
    const personId = req.params.personId;
    const dataOfPerson = cache.get(personId)

    if (dataOfPerson === null) {
        Person.findById(personId, (err, response) => {
            if (err === null) {
                res.status(200).json({
                    message: 'Gettin record from database',
                    data: response,
                });
                cache.put(personId, response);
                console.log(response);
            } else {
                res.status(500).json({
                    message: 'Data not found',
                    error: err
                });
            }
        })
    } else {
        res.status(200).json({
            message: 'Getting record from cache',
            data: dataOfPerson
        });
    }
});

/**
 * Method to delete a record
 */
router.delete('/:personId', checkAuth, (req, res, next) => {
    const id = req.params.personId;

    Person.remove({
        _id: id
    }, err => {
        if (err === null) {
            res.status(200).json({
                message: 'Record was deleted'
            });
        } else {
            res.status(500).json({
                message: 'Something wen\'t wrong'
            });
        }
    });
});

/**
 * Method to editing a record
 */
router.put('/:personId', checkAuth, (req, res, next) => {
    const personId = req.params.personId;
    const personBody = {
        name: req.body.name,
        age: req.body.age,
        email: req.body.email,
        phone: req.body.phone
    }

    const personSchema = {
        "properties": {
            name: {
                "type": "string",
                "minLength": 1,
                "maxLength": 30,
                "pattern": "^[a-zA-Z\\s]+$"
            },
            age: {
                "type": "number",
                "minimum": 0,
                "maximum": 200
            },
            email: {
                "type": "string",
                "pattern": "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
            },
            phone: {
                "type": "string",
                "pattern": "^\\(?([0-9]{3})\\)?([ .-]?)([0-9]{3})\\2([0-9]{3})$"
            }
        },
        "required": ["name", "age", "email"]
    }

    const validate = ajv.compile(personSchema);
    const valid = validate(personBody);

    if (ajv.errorsText(validate.errors) === "No errors") {
        Person.update({
            _id: personId
        }, {
            name: personBody.name,
            age: personBody.age,
            email: personBody.email,
            phone: personBody.phone
        }, (err, raw) => {
            if (err === null) {
                res.status(200).json({
                    message: "Record has been edited"
                })
            } else {
                res.status(500).json({
                    message: 'Something wen\'t wrong'
                });
            }
        })
    } else {
        res.status(500).json({
            message: ajv.errorsText(validate.errors)
        });
    }
})

module.exports = router;