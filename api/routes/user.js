const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

router.post('/', (req, res, next) => {
    try {
        const token = jwt.sign({
                name: "Andrzej Matysiak",
                age: "24",
                email: "andrzej.m93@o2.pl"
            },
            "secretKey");

        res.status(200).json({
            message: "Token created",
            token: token
        })
    } catch (error) {
        res.status(404).json({
            message: "Token wasn't be created"
        })
    }
})

module.exports = router;